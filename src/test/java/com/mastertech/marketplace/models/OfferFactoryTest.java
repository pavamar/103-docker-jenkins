package com.mastertech.marketplace.models;

import static org.junit.Assert.*;

import org.junit.Test;

public class OfferFactoryTest {

	@Test
	public void build_accetable_offer() throws InvalidOffer {
		
		Offer offerPrototype = new Offer();
		Product product = new Product();
		product.setPrice(100);
		offerPrototype.setProduct(product);
		offerPrototype.setBid(70.0);
	
		OfferFactory factory = new OfferFactory();
		Offer offer = factory.completeOffer(offerPrototype);
		assertFalse(offer.isApproved());
	}
	
	@Test(expected=InvalidOffer.class)
	public void not_build_an_unaccetable_offer() throws InvalidOffer {
		
		Offer offerPrototype = new Offer();
		Product product = new Product();
		product.setPrice(100);
		offerPrototype.setProduct(product);
		offerPrototype.setBid(30.0);
	
		OfferFactory factory = new OfferFactory();
		Offer offer = factory.completeOffer(offerPrototype);
		assertFalse(offer.isApproved());
	}

}
