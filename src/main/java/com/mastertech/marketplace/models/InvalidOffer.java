package com.mastertech.marketplace.models;

public class InvalidOffer extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidOffer(String msg) {
		super(msg);
	}

}
